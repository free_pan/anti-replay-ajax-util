# anti-replay-ajax-util

基于umi-request, 在ajax请求层面, 实现请求防重.

支持的两种防重模式: `丢弃旧请求`, `丢弃新请求`

## 使用

```
yarn add anti-replay-ajax-util
```

### 自定义系统的Ajax请求帮助类

```tsx
// SystemAjaxUtil.ts
import {AjaxUtil, umiRequest, DISCARD_NEW, DISCARD_OLD} from "anti-replay-ajax-util";
import {StorageUtil} from "pzy-storage-util";
import {LocalStorageKey} from "@/utils/SystemConstantUtil";

// 加入请求拦截器(为每个请求加上额外请求头:Authorization)
umiRequest.interceptors.request.use((url, options) => {
    const obj: any = StorageUtil.localOpt.getJson(LocalStorageKey.LoginUser);
    console.log('请求拦截器:', options, obj);
    const customHeader: any = {}
    if (obj?.token) {
        customHeader.Authorization = obj.token;
    }
    const headers = {...options.headers, ...customHeader};
    console.log('headers', headers)
    return (
        {
            url: url,
            options: {...options, headers, interceptors: true},
        }
    );
});

export const ajaxUtilInstance = new AjaxUtil(umiRequest);

// @ts-ignore
export const AntiReplayModel = {DISCARD_NEW, DISCARD_OLD}
```

### 使用

```typescript
import {ajaxUtilInstance, AntiReplayModel} from "../utils/SystemAajaxUtil";

async function login(params: LoginParam): Promise<WebApiResp<LoginRespInfo>> {
    // @ts-ignore
    const resp: WebApiResp<LoginRespInfo> = await ajaxUtilInstance.postBody<WebApiResp<LoginRespInfo>>({
        url: '/webapi/auth/login',
        data: params
    }, AntiReplayModel.DISCARD_NEW);
    console.log('resp', resp.data)
    return resp;
}
```

### 全局拦截器

```typescript
import {AjaxUtil, WebApiData,umiRequest} from 'anti-replay-ajax-util'

// request拦截器, 改变url 或 options.
umiRequest.interceptors.request.use((url, options) => {
  return (
    {
      url: `${url}&interceptors=yes`,
      options: { ...options, interceptors: true },
    }
  );
});

// 和上一个相同
umiRequest.interceptors.request.use((url, options) => {
  return (
    {
      url: `${url}&interceptors=yes`,
      options: { ...options, interceptors: true },
    }
  );
}, { global: true });


// response拦截器, 处理response
umiRequest.interceptors.response.use((response, options) => {
  const contentType = response.headers.get('Content-Type');
  return response;
});

// 提前对响应做异常处理
umiRequest.interceptors.response.use((response) => {
  const codeMaps = {
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
  };
  console.error(codeMaps[response.status]);
  return response;
});

// 克隆响应对象做解析处理
umiRequest.interceptors.response.use(async (response) => {
  const data = await response.clone().json();
  if(data && data.NOT_LOGIN) {
    location.href = '登录url';
  }
  return response;
})

AjaxUtil.post({
    url:'web api url',
    umiRequest:umiRequest
},AjaxUtil.AntiReplayModel.DISCARD_NEW)
```
