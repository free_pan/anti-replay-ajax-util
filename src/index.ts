import umiRequest, {AjaxUtil, AjaxRequestParams, DISCARD_OLD, DISCARD_NEW} from './AjaxUtil'

export {AjaxUtil, AjaxRequestParams, umiRequest, DISCARD_OLD, DISCARD_NEW}
